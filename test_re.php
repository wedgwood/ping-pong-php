<?php
/* $matches = array();
$x = '/(cde)|(abc(e))/';
preg_match_all($x, 'aabcexxxcde', $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
print_r($matches);

die(); */
$route = '/a1a/b1b/c1c/\\\\<abc:int:\\>\\>>/e1e/<name:re:abc>/\<d1d::a>';
// $route = '/abc/';
echo 'route:   ' . $route . "\n";

$matches = array();
$parse_rule =
'(\\\\*)<([a-zA-Z_][\w]*)?(?::([\w]*)(?::((?:\\\\.|[^\\\\>]+)+)?)?)?>';
/* '(\\\\*)(?:<([a-zA-Z_][\w]*)?(?::([\w]*)(?::((?:\\\\.|[^\\\\>]+)+)?)?)?>)';  */
/* $parse_rule =
'(\\\\*)(?:(?:<([a-zA-Z_][\w]*)?(?::([\w]*)(?::((?:[^(?<=\\\\)>]+)+)?)?)?>))'; */
echo 'parse rule:  ' . $parse_rule . "\n";

$ret = preg_match_all(
    '/' . $parse_rule . '/',
    $route,
    $matches,
    PREG_SET_ORDER | PREG_OFFSET_CAPTURE
);

// var_dump($matches);


$synPartial = array();
$defaultFilter = 're';

$prefix = '';
$offset = 0;

foreach ($matches as $match) {
    $prefix .= substr($route, $offset, $match[0][1] - $offset);

    if (strlen($match[1][0]) % 2) {
        $prefix .= $match[0][0];
        echo "\n-----\n";
        var_dump($prefix);
        echo "\n-----\n";
        $offset = $match[0][1] + strlen($match[0][0]);
        continue;
    }

    if ($prefix) {
        $synPartial[] = array(
            'key'      => $prefix,
            'filter'    => null,
            'conf'      => null
        );
    }

    $synPartial[] = array(
        'key'      => $match[2][0],
        'filter'    => isset($match[3]) ? $match[3][0] : 're',
        'conf'      => isset($match[4]) ? $match[4][0] : null
    );

    $prefix = '';
    $offset = $match[0][1] + strlen($match[0][0]);
}

echo "\nlllllllllllllllllllllllllllllll\n";
var_dump($offset);
var_dump($route);
echo "\nlllllllllllllllllllllllllllllll\n";

if ($offset < strlen($route) - 1 || $prefix) {
    echo '----' . "\n";
    var_dump('' . substr($route, $offset));
    var_dump($prefix . substr($route, $offset));
    echo "\n" . '----';
    $synPartial[] = array(
        'key'  =>  substr($route, $offset),
        'filter' => null,
        'conf'  => null
    );
}


// die();

// var_dump($synPartial);

$reFilter = 're';
$defaultPattern = '[^/]+';

function reFilter($conf) {
    global $defaultPattern;
    return array($conf ? $conf : $defaultPattern, null);
}

$toInt = function($in) {
    return (int)$in;
};

function intFilter() {
    global $toInt;
    return array('-?\d+', $toInt);
}

$toFloat = function($in) {
    return (float)$in;
};

function floatFilter() {
    global $toFloat;
    return array('-?[\d\.]+', $toFloat);
}

function pathFilter() {
    return array('.+?', null);
}

echo "\n====================================================\n";
// var_dump($synPartial);

$isStatic = true;
$filters = array(
    're'    => 'reFilter',
    'int'   => 'intFilter',
    'float' => 'floatFilter',
    'path'  => 'pathFilter'
);

$pattern = '';
$inFilters = array();
$matchHandlers = array();

foreach ($synPartial as $syn) {
    if ($syn['filter']) {
        $isStatic = false;
        $filter = $filters[$syn['filter']]($syn['conf']);

        if ($syn['key']) {
            $pattern .= sprintf('(?P<%s>%s)', $syn['key'], $filter[0]);
        } else {
            $pattern .= sprintf('(?:%s)', $syn['key'], $filter[0]);
        }

        if (isset($filter[1])) {
            $inFilters[$syn['key']] = $filter[1];
        }
    } elseif ($syn['key']) {
        $pattern .= preg_quote($syn['key']);
        // $pattern .= $syn['key'];
    }
}

$re = sprintf('^(%s)$', $pattern);
var_dump($re);

$matchHandlers[] = function($path) use($re, $inFilters) {
    $matches = array();
    $args = array();
    $ret = preg_match('#' . $re . '#', $path, $matches, PREG_PATTERN_ORDER);

    foreach ($inFilters as $key => $filter) {
        if ($filter) {
            $args[$key] = $filter($matches[$key]);
        } else {
            $args[$key] = $matches[$key];
        }
    }

    return $args;
};

var_dump($matchHandlers[0]('/a1a/b1b/c1c/777/e1e/abc/d1d/f1f/'));
// print_r($synPartial);
