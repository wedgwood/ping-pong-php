<?php
namespace Turbine\Core;

class DictBag extends \ArrayObject
{
    public function __construct($data)
    {
        if (is_null($data)) {
            $data = array();
        }

        parent::__construct($data);
    }

    public function setData($data)
    {
        parent::__construct($data);
    }

    public function get($key, $default = null)
    {
        return $this->offsetExists($key) ? $this[$key] : $default;
    }

    public function set($key, $value)
    {
        $this[$key] = $value;
        return $this;
    }

    public function has($key)
    {
        return $this->offsetExists($key);
    }

    public function merge($data)
    {
        foreach ($data as $key => $value) {
            $this[$key] = $value;
        }

        return $this;
    }
}
