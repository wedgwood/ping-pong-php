<?php
class GSR
{
    private static $data_ = array();

    public function has($key) {
        return isset(self::$data_[$key]);
    }

    public function set($key, $value) {
        self::$data_[$key] = $value;
    }

    public function get($key, $otherwise = null) {
        return isset(self::$data_[$key]) ?
                self::$data_[$key] : $otherwise;
    }

    public function remove($key) {
        unset(self::$data_[$key]);
    }

    public function pop($key, $otherwise = null) {
        $ret = self::get($key, $otherwise);
        self::remove($key);
        return $ret;
    }
}
