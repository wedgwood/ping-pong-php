<?php
namespace Turbine\Core;

class Turbine
{
    private static $instance_ = null;
    private static $default_error_types = E_ALL;

    public static function getInstance()
    {
        if (is_null(self::$instance_)) {
            self::$instance_ = self::create();
        }

        return self::$instance_;
    }

    public static function create()
    {
        return new static();
    }

    public function __destruct()
    {
        $this->fire(Event::ON_UNINIT());
    }

    private $observer_list_ = array();

    public function bootstrap()
    {
        $this->fire(Event::ON_INIT());
    }

    public function register($e, $handler)
    {
        if (!isset($this->observer_list_[$e])) {
            $this->observer_list_[$e] = array();
        }

        if (!is_callable($handler)) {
            // error();
            return $this;
        }

        $this->observer_list_[$e][] = $handler;

        return $this;
    }

    public function unregister($event_type, $which = null)
    {
        if (is_null($which)) {
            unset($this->observer_list_[$event_type]);
        } else {
            foreach ($this->observer_list_[$event_type] as $key => $handler) {
                if ($which == $handler) {
                    unset($this->observer_list_[$key]);
                }
            }
        }

        return $this;
    }

    public function fire(Event $e)
    {
        $e_type = $e->getType();
        $data = $e->getData();

        if (isset($this->observer_list_[$e_type])) {
            $observer_list = $this->observer_list_[$e_type];
            $is_continued = false;

            foreach ($observer_list as $observer) {
                $is_continued = $observer($e, $data);

                if (!$is_continued || $e->isStopped()) {
                    break;
                }
            }
        }

        return $this;
    }

    public function fireOne(Event $event)
    {
        $event_type = $event->getType();
        $data = $event->getData();

        // TODO: event straegy
        if (isset($this->observer_list_[$event_type])) {
            $observer_list = $this->observer_list_[$event_type];
            // $fire_ret = true;

            $observer_list[0]($event, $data);
        }
    }

    public function error($error_handler, $error_types = null)
    {
        $error_types = $error_types ? $error_types : self::$default_error_types;

        return set_error_handler($error_handler, $error_types);
    }
}
