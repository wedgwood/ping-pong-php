<?php
require_once '../core/Turbine.class.php';
require_once '../web/HttpRequest.class.php';
require_once '../web/WebRoute.class.php';
require_once '../web/WebRouter.class.php';

class MatchTest extends PHPUnit_Framework_TestCase {
    public function testIntMatch() {
        $route = new WebRoute(
            '/guides/<id:int>/',
            function() {
                echo 'haode';

            },
            'get'
        );

        $router = new WebRouter();
        $match = $router->add($route);

        $ret = $match('/guides/123/');

        $this->assertEquals($ret['id'], 123);
    }

    public function testFloatMatch() {
        $route = new WebRoute(
            '/guides/<id:float>/',
            function() {
                echo 'haode';

            },
            'get'
        );

        $router = new WebRouter();
        $match = $router->add($route);

        $ret = $match('/guides/123.1/');

        $this->assertEquals($ret['id'], 123.1);
    }

    public function testReMatch() {
        $route = new WebRoute(
            '/guides/<id:re:\d+>/',
            function() {
                echo 'haode';
            },
            'get'
        );

        $router = new WebRouter();
        $match = $router->add($route);

        $ret = $match('/guides/123/');
        $this->assertEquals($ret['id'], 123);
    }

    public function testMatch() {
        $route = new WebRoute(
            '/guides/<id:re:\d+>/',
            function() {
                echo 'haode';
            },
            'get'
        );

        $router = new WebRouter();
        $match = $router->add($route);
        $router->match('/guides/123/');

        $this->assertTrue(true);
    }
}
