<?php
namespace Turbine\Web\Response;

function JSONResponse($obj, $status = 200, $headers = null) {
    return new HTTPResponse(json_encode($obj), $status, $headers);
}
