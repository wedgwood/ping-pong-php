<?php
namespace Turbine\Web\Response;

class HttpResponse extends \Exception {
    private $output_;
    private $status_line_;
    private $status_;
    private $headers_;

    public static $default_content_type = 'text/html; charset=UTF-8';

    public function __construct($output = '', $status = 200, $headers = null) {
        $this->status_ = $status;
        $this->output_ = $output;
        $this->headers_ = $headers;
    }

    public function __toString() {
        return $this->output_;
    }

    public function getHeaders() {
        return $this->headers_;
    }

    public function getStatus() {
        return $this->status_;
    }

    public function merge(HttpResponse $response) {
        $headers = $response->getHeaders();

        foreach ($headers as $key => $val) {
            $this->headers_[$key] = $val;
        }

        $this->status_ = $response->getStatus();

        return $this;
    }

    public function append($append) {
        $this->output_ .= $append;
        return $this;
    }
}
