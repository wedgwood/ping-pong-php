<?php
namespace Turbine\Web\Response;

class JSONResponse extends HttpResponse
{
    public function __construct($obj, $status = 200, $headers = null)
    {
        parent::__construct(json_encode($obj), $status, $headers);
    }
}
