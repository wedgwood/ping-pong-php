<?php
namespace Turbine\Web\Response;

class ErrorResponse extends HttpResponse
{
    private $exception_;
    private $traceback_;

    public function __construct(
        $status = 500,
        $output = '',
        $exception = null,
        $traceback = null,
        $headers = null
    ) {
        parent::__construct($output, $status, $headers);

        $this->traceback_ = $traceback;
        $this->exception_ = $exception;
    }

    public function __toString()
    {
        // TODO: some control given to user
        return (string)$this->getStatus();
    }
}
