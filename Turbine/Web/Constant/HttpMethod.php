<?php
namespace Turbine\Web\Constant;

class HttpMethod
{
    const GET       = 'GET';
    const POST      = 'POST';
    const HEAD      = 'HEAD';
    const DELETE    = 'DELETE';
    const PUT       = 'PUT';
    const ANY       = 'ANY';
}
