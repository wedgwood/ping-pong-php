<?php
namespace Turbine\Web\Constant;

class HttpScheme
{
    const HTTP = 'http';
    const HTTPS = 'https';
}
