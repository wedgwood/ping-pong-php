<?php
namespace Turbine\Web\Event;

class EventData
{
    public $request;
    public $response;
    public $e;
    public $u_data;
}
