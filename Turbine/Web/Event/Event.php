<?php
namespace Turbine\Web\Event;

class Event extends \Turbine\Core\Event
{
    const TYPE_ON_REQUEST   = 'OnRequest';
    const TYPE_BEFORE_ROUTE = 'BeforeRoute';
    const TYPE_AFTER_ROUTE  = 'AfterRoute';
    const TYPE_BEFORE_HANDLE_REQUEST = 'BeforeHandleRequest';
    const TYPE_AFTER_HANDLE_REQUEST  = 'AfterHandleRequest';
}
