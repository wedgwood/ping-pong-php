<?php
namespace Turbine\Web\Template;

interface ITemplate
{
    public function assign($var);

    public function fetch();

    public function display();

    public function isCached();
}

class Tempalte implements ITemplate
{
    public function assign($var) {

    }
}
