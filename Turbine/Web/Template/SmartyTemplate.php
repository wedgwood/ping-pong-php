<?php
namespace Turbine\Web\Template;

class SmartyTemplate implements ITemplate
{
    public function __construct()
    {
    }

    public function assign($name, $value)
    {
    }

    public function fetch($tpl)
    {
    }

    public function display($tpl)
    {
    }
}
