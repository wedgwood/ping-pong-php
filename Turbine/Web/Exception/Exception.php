<?php
namespace Turbine\Web\Exception;

class WebSessionException extends Exception {}
class WebRouteException extends Exception {}
class WebRouterSyntaxException extends WebRouteException{}
