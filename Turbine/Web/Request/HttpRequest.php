<?php
namespace Turbine\Web\Request;

use Turbine\Web\Constant\HttpScheme;
use Turbine\Web\Constant\HttpMethod;
use Turbine\Core\DictBag;
use Turbine\Core\DictBagRO;

class HttpRequest
{
    private $http_host_;

    private $header_;
    private $session_;
    private $cookie_;

    private $query_data_;
    private $post_data_;
    private $reqeust_data_;
    private $put_data_;
    private $row_put_data_;

    private $http_scheme_;
    private $path_info_;

    private $is_xhr_;
    private $is_flash_request_;

    private $is_delete_;
    private $is_get_;
    private $is_head_;
    private $is_options_;
    private $is_post_;
    private $is_put_;

    public function __construct($env = null)
    {
        if (isset($mock_env)) {
            $this->request_data_    = new DictBagRO($mock_env['request']);
            $this->query_data_      = new DictBagRO($mock_env['query']);
            $this->post_data_       = new DictBagRO($mock_env['post']);
            $this->server_          = new DictBagRO($mock_env['server']);
            $this->headers_         = new DictBag();
        } else {
            $this->request_data_    = new DictBagRO($_REQUEST);
            $this->query_data_      = new DictBagRO($_GET);
            $this->post_data_       = new DictBagRO($_POST);
            $this->server_          = new DictBagRO($_SERVER);
            $this->headers_         = new HttpHeader($this->server_);
        }
    }

    public function getHttpHost()
    {
        if (is_null($this->http_host_)) {
            $host = $this->getServer('HTTP_HOST');

            if (!empty($host)) {
                $this->http_host_ = $host;
            } else {
                $scheme = $this->getScheme();
                $name = $this->getServer('SERVER_NAME');
                $port = $this->getServer('SERVER_PORT');

                if (is_null($name)) {
                    $this->http_host_ = '';
                } elseif ((HttpScheme::HTTP == $scheme && 80 == $port)
                            || (HttpScheme::HTTPS == $scheme && 443 == $port)) {
                    $this->http_host_ = $name;
                } else {
                    $this->http_host_ = $name . ':' . $port;
                }
            }
        }

        return $this->http_host_;
    }

    public function getHeader($key, $default = null)
    {
        static $special_keys = array(
            'CONTENT_LENGTH',
            'CONTENT_TYPE',
            'PHP_AUTH_USER',
            'PHP_AUTH_PW',
            'PHP_AUTH_DIGEST',
            'AUTH_TYPE'
        );

        if ($key) {
            $key = strtoupper($key);

            if (!in_array($key, $special_keys)) {
                $key = 'HTTP_' . str_replace('-', '_', $key);
            }

            if (!array_key_exists($key, $this->header_)) {
            }

            return $this->header_[$key];
        }

        switch ($key) {
        case null:
            static $flag = false;

            if ($flag) {
                return $this->headers_;
            }

        }

        $name = 'HTTP_' . strtoupper(str_replace('-', '_', $key));
        $value = $this->getServer($name, false);
    }

    public function getQuery($key, $default = null)
    {
        if (is_null($key)) {
            return $this->query_data_;
        }

        return $this->query_data_->get($key, $default);
    }

    public function getPost($key, $default = null)
    {
        if (is_null($key)) {
            return $this->post_data_;
        }

        return $this->post_data_->get($key, $default);
    }

    public function getPutRow()
    {
        if (is_null($this->row_put_data_)) {
            $row_put_data = file_get_contents('php://input');

            if (empty($row_put_data)) {
                $this->row_put_data_ = false;
            } else {
                $this->row_put_data_ = $row_put_data;
            }
        }

        return $this->row_put_data_;
    }

    public function getPut($key, $default = null)
    {
        if (is_null($this->put_data_)) {
            $row_put_data = $this->getPutRow();

            if ($row_put_data) {
                $this->put_data_ = new DictBag();
            } else {
                mb_parse_str($row_put_data, $output);
                $this->put_data_ = new DictBag($output);
            }
        }

        if (is_null($key)) {
            return $this->put_data_;
        }

        return $this->post_data_->get($key, $default);
    }

    public function getServer($key, $default = null)
    {
        if (is_null($key)) {
            return $this->server_;
        }

        return $this->server_->get($key, $default);
    }

    public function getMethod()
    {
        return $this->getServer('REQUEST_METHOD');
    }

    public function isPost()
    {
        if (!isset($this->is_post_)) {
            $this->is_post_
                = (HttpMethod::POST == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_post_;
    }

    public function isGet()
    {
        if (!isset($this->is_get_)) {
            $this->is_get_
                = (HttpMethod::GET == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_get_;
    }

    public function isPut()
    {
        if (!isset($this->is_put_)) {
            $this->is_put_
                = (HttpMethod::PUT == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_put_;
    }

    public function isDelete()
    {
        if (!isset($this->is_delete_)) {
            $this->is_delete_
                = (HttpMethod::DELETE == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_delete_;
    }

    public function isHead() {
        if (!isset($this->is_head_)) {
            $this->is_head_
                = (HttpMethod::HEAD == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_head_;
    }

    public function isOptions()
    {
        if (!isset($this->is_options_)) {
            $this->is_options_
                = (HttpMethod::OPTION == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_options_;
    }

    public function isXHR()
    {
        if (!isset($this->is_xhr_)) {
            $this->is_xhr_
                = ('XMLHttpRequest'
                    == strtolower($this->getHeader('X_REQUESTED_WITH')));
        }

        return $this->is_xhr_;
    }

    public function isAjax()
    {
        return $this->isXHR();
    }

    public function isFlashRequest()
    {
        if (!isset($this->is_flash_request_)) {
            $user_agent = strtolower($this->getHeader('User-Agent'));
            $this->is_flash_request_
                = (false === strpos($header, 'flash') ? false : true);
        }

        return $this->is_flash_request_;
    }

    public function getClientIp($check_proxy = true)
    {
        return $ip;
    }

    public function getScheme()
    {
        if (is_null($this->http_scheme_)) {
            $this->http_scheme_ = ($this->getServer('HTTPS') == 'on') ?
                                    HttpScheme::HTTPS : HttpScheme::HTTP;
        }

        return $this->http_scheme_;
    }

    public function isSecure()
    {
        static $is_secure = null;

        if (is_null($is_secure)) {
            $is_secure = ($this->getScheme() == HttpScheme::HTTPS);
        }

        return $is_secure;
    }

    // maybe not compatitable for all kinds of servers
    public function getPathInfo()
    {
        $this->path_info_ = $this->getServer('PATH_INFO');

        if (is_null($this->path_info_)) {
            $query_string = $this->getServer('QUERY_STRING');
            $request_uri = $this->getServer('REQUEST_URI');

            $tmp = null;

            if ($query_string) {
                $qs_pos = strpos($request_uri, $query_string);
                $tmp = substr($request_uri, 0, $qs_pos - 2);
            } else {
                $tmp = $request_uri;
            }

            $this->path_info_ = substr($tmp, strlen($request_uri) + 1);
        }

        return $this->path_info_;
    }
}
