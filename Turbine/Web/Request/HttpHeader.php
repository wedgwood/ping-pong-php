<?php
namespace Turbine\Web\Request;

class HttpHeader extends \Turbine\Core\DictBag {
    private $server_;

    private static $key_map_ = array(
        'Accept'            => 'HTTP_ACCEPT',
        'Accept-Charset'    => 'HTTP_ACCEPT_CHARSET',
        'Accept-Encoding'   => 'HTTP_ACCEPT_ENCODING',
        'Accept-Language'   => 'HTTP_ACCEPT_LANGUAGE',
        'Cache-Control'     => 'HTTP_CACHE_CONTROL',
        'Connection'        => 'HTTP_CONNECTION',
        'Content-Length'    => 'CONTENT_LENGTH',
        'Content-Type'      => 'CONTENT_TYPE',
        'Cookie'            => 'HTTP_COOKIE',
        'Host'              => 'HTTP_HOST',
        'User-Agent'        => 'HTTP_USER_AGENT',
        'Referer'           => 'HTTP_REFERER',
        'HTTP_X_REQUESTED_WITH' => 'HTTP_X_REQUESTED_WITH'
    );

    private $http_request_;

    public function __construct($server) {
        $this->server_ = $server;
    }

    /**
     * get
     *
     * @return void
     * @author Yuan B.J.
     */
    public function get($key, $value = null) {
        if (parent::has($key)) {
            return parent::get($key, $value);
        }

        if (array_key_exists($key, self::$key_map_)) {
            $exact_key = self::$key_map_[$key];
            $ret = $this->server_->get($exact_key);
            parent::set_($exact_key, $value);

            return $ret;
        }

        return $value;
    }
}
