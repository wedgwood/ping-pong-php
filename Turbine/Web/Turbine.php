<?php
namespace Turbine\Web;

use Turbine\Core;

use Turbine\Web\Event\Event;
use Turbine\Web\Event\EventData;

use Turbine\Web\Router\Route;
use Turbine\Web\Router\Router;

use Turbine\Web\Request\HttpRequest;

use Turbine\Web\Response\HttpResponse;
use Turbine\Web\Response\JSONResponse;
use Turbine\Web\Response\ErrorResponse;

class Turbine extends Core\Turbine
{
    private $auto_json_ = true;
    private $router_;
    private $routes_ = array();
    private $status_handler_ = array();

    public function __construct()
    {
        $this->router_ = new Router();
    }

    public function bootstrap()
    {
        parent::bootstrap();

        $request = new HttpRequest();
        $response = null;
        $data = new EventData();

        try {
            $data->request = $request;

            $this->fire(Event::ON_REQUEST($data));
            $this->fire(Event::BEFORE_ROUTE($data));

            $pair = $this->router_->match($request);

            $this->fire(Event::AFTER_ROUTE($data));
            $this->fire(Event::BEFORE_HANDLE_REQUEST($data));

            if (isset($pair) || isset($pair[0])) {
                $params = null;

                if ($pair[1]) {
                    $params = $pair[1];
                }

                $params[] = $request;
                $response = call_user_func_array($pair[0], $params);

                if (is_array($response)) {
                    if ($this->auto_json_) {
                        $response = new JSONResponse($response);
                    }
                }
            } else {
                $response = new ErrorResponse(404);
            }
        } catch (HttpResponse $response) {
            $response = $response;
        } catch (Exception $e) {
            $e_data->e = $e;
            $this->fire(Event::ON_ERROR($data));
            $response = new ErrorResponse(500, '', $e);
        }

        $data->response = $response;
        $this->fire(Event::AFTER_HANDLE_REQUEST($data));

        $this->output($response);
    }

    public function output($response)
    {
    }

    public function addRoute(Route $route)
    {
        $this->routes_[] = $route;
        $this->router_->add($route);

        return $this;
    }

    public function route(
        $path,
        $method,
        $callable,
        $name = null,
        $config = null
    ) {
        if (!is_array($path)) {
            $path = array($path);
        }

        if (!is_array($method)) {
            $method = array($method);
        }

        foreach ($path as $rule) {
            foreach ($method as $verb) {
                $verb = strtoupper($verb);
                $route = new Route($rule, $verb, $callable, $name, $config);
                $this->addRoute($route);
            }
        }

        return $this;
    }

    public function get(
        $path,
        $callable,
        $name = null,
        $config = null
    ) {
        return $this->route($path, 'GET', $callable, $name, $config);
    }

    public function post(
        $path,
        $callable,
        $name = null,
        $config = null
    ) {
        return $this->route($path, 'POST', $callable, $name, $config);
    }

    public function put(
        $path,
        $callable,
        $name = null,
        $config = null
    ) {
        return $this->route($path, 'PUT', $callable, $name, $config);
    }

    public function delete(
        $path,
        $callable,
        $name = null,
        $config = null
    ) {
        return $this->route($path, 'DELETE', $callable, $name, $config);
    }

    public function status()
    {

    }

    public function exception()
    {

    }
}
