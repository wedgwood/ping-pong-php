<?php
namespace Turbine\Web\Router;

use Turbine\Web\Request\HttpRequest;

class Router
{
    private $rules_ = array();
    private $static_ = array();
    private $dynamic_ = array();

    private $rule_syntax_ = <<<EOF
#(\\\\*)<([a-zA-Z_][\w]*)?(?::([\w]*)(?::((?:\\\\.|[^\\\\>]+)+)?)?)?>#
EOF;

    private $default_filter_ = 're';
    private $default_pattern_ = '[^/]+';

    const MAX_PATTERN_LEN_ = 65535;

    private $filters_ = array();

    ///TODO: deal with trailing slash
    private $auto_trailing_slash_ = false;

    public function setAutoTrailing($auto_trailing_slash)
    {
        if (is_null($auto_trailing_slash)) {
            return $this->auto_trailing_slash;
        }

        $this->auto_trailing_slash_ = $auto_trailing_slash;
    }

    const PATTERN_MAX_LEN_ = 65535;

    public function __construct()
    {
        $default_pattern = $this->default_pattern_;

        $this->filters_ = array(
            're' => function($conf) use($default_pattern) {
                return array(
                    $conf ? $conf : $default_pattern,
                    null,
                    null
                );
            },
            'int' => function($conf) {
                return array(
                    '-?\d+',
                    'intval',
                    null
                );
            },
            'float' => function($conf) {
                return array(
                    '-?[\d\.]+',
                    'floatval',
                    null
                );
            }
        );
    }

    public function addFilter($name, $callable)
    {
        // callable must return array(re, func, config)
        $this->filters_[$name] = $callable;
    }

    private function parseRule_($rule)
    {
        $reg_result = preg_match_all(
                            $this->rule_syntax_,
                            $rule,
                            $matches,
                            PREG_SET_ORDER | PREG_OFFSET_CAPTURE
                        );

        $syn_partials = array();
        $prefix = '';
        $offset = 0;

        foreach ($matches as $match) {
            $prefix .= substr($rule, $offset, $match[0][1] - $offset);

            $partial = $match[0][0];
            $partial_len = strlen($partial);
            $partial_begin = $match[0][1];

            if ($partial_begin % 2) {
                $prefix .= $partial;
                $offset = $partial_begin + $partial_len;
                continue;
            }

            if ($prefix) {
                $syn_partials[] = array(
                    'key' => $prefix,
                    'filter' => null,
                    'conf' => null
                );
            }

            $syn_partials[] = array(
                'key'
                    => $match[2][0],
                'filter'
                    => isset($match[3]) ? $match[3][0] : $this->default_filter_,
                'conf'
                    => isset($match[4]) ? $match[4][0] : null
            );

            $prefix = '';
            $offset = $match[0][1] + strlen($match[0][0]);
        }

        if ($offset < strlen($rule) || $prefix) {
            $syn_partials[] = array(
                'key' => $prefix . substr($rule, $offset),
                'filter' => null,
                'conf' => null
            );
        }

        return $syn_partials;
    }

    public function add(Route $route)
    {
        $rule = $route->rule;
        $method = $route->method;

        if (in_array($rule, $this->rules_)) {
            $this->rules_[$rule][$method] = $route;
            return;
        }

        $syn_partials = $this->parseRule_($rule);

        $pattern = '';
        $is_static = true;
        $filters = array();
        $this->rules_[$rule] = array($method => $route);
        $target = &$this->rules_[$rule];

        foreach ($syn_partials as $syn_partial) {
            // $key, $mode, $conf
            $key = $syn_partial['key'];
            $mode = $syn_partial['filter'];
            $conf = $syn_partial['conf'];

            if ($mode) {
                $is_static = false;

                $filter = $this->filters_[$mode]($conf);

                if ($key) {
                    $pattern .= sprintf('(?P<%s>%s)', $key, $filter[0]);
                } else {
                    $pattern .= sprintf('(?:%s)', $filter[0]);
                }

                if ($filter[1]) {
                    $filters[$key] = $filter[1];
                }
            } elseif ($key) {
                $pattern .= preg_quote($key);
            }
        }

        if ($is_static) {
            $this->static_[$rule] = &$target;
            return;
        }

        $re = '#^' . $pattern . '$#';

        $get_url_args = function($path) use($re, $filters) {
            $ret = preg_match($re, $path, $url_args);
            unset($url_args[0]);

            foreach ($filters as $key => $filter) {
                $url_args[$key] = $filter($url_args[$key]);
            }

            return $url_args;
        };

        $matchonly_pattern = preg_replace_callback(
                                '#(\\\\*)(\\(\\?P<[^>]*>|\\((?!\\?))#',
                                function($m) {
                                    if (strlen($m[1]) % 2) {
                                        return $m[0];
                                    }

                                    return $m[1] . '(?:';
                                },
                                $pattern
                            );

        $dynamic_end = count($this->dynamic_) - 1;
        $tmp = array($get_url_args, $target);

        if ($dynamic_end < 0) {
            $this->dynamic_[0]
                = array('#(^' . $matchonly_pattern . '$)#', array($tmp));
        } else {
            $combined = rtrim($this->dynamic_[$dynamic_end][0], '#')
                        . '|(^' . $matchonly_pattern . '$)#';

            if (strlen($combined) <= self::MAX_PATTERN_LEN_) {
                $this->dynamic_[$dynamic_end][0] = $combined;
                $this->dynamic_[$dynamic_end][1][] = $tmp;
            } else {
                $this->dynamic_[++ $dynamic_end]
                    = array('#(^' . $matchonly_pattern . '$)#', array($tmp));
            }
        }

        return $get_url_args;
    }

    public function match(HttpRequest $request)
    {
        $path_info = $request->getPathInfo();

        $url_args = array();
        $targets = null;

        if (isset($this->static_[$path_info])) {
            $route = $this->static_[$path_info];
        } else {
            foreach ($this->dynamic_ as $entry) {
                $combined_re = $entry[0];
                $match_times = preg_match($combined_re, $path_info, $match);

                if (!$match_times) {
                    continue;
                }

                $max_index = count($match) - 2;
                list($get_url_args, $targets) = $entry[1][$max_index];

                if ($get_url_args) {
                    $url_args = $get_url_args($path_info);
                }

                break;
            }
        }

        $method = $request->getMethod();

        if (!$targets) {
            return null;
        } elseif (isset($targets[$method])) {
            return array($targets[$method], $url_args);
        } elseif (HttpMethod::HEAD == $method && isset($targets[$method])) {
            return array($targets[HttpMethod::GET], $url_args);
        } elseif (isset($targets[HttpMethod::ANY])) {
            return array($targets[HttpMethod::ANY], $url_args);
        } else {
            // TODO: for option
        }

        // TODO: use specified exception 405
        throw new Exception();
    }
}
