<?php
require './Core/ClassLoader.php';

use Turbine\Web\Constant\HttpMethod;

$cl = new ClassLoader('Turbine', dirname(__DIR__));
$cl->register();

$web_turbine = Turbine\Web\Turbine::getInstance();

$web_turbine->register('init', function() {
    echo 'init';
})->register('uninit', function(){
    echo 'uninit';
})->unregister('init')->route(
    '/guides/<id:int>/',
    HttpMethod::GET,
    function($id) {
        echo "id -> $id";
        var_dump('guide');
    }
)->route(
    '/places/<id:int>/',
    HttpMethod::GET,
    function($id) {
        echo "id -> $id";
        var_dump('place');
    }
)->route(
    '/user/<id:int>/',
    HttpMethod::GET,
    function ($id) {
        echo "id -> $id";
        var_dump('user');
    }
)->error(function($errno, $errstr, $errfile, $errline, $errcontext) {
    printf(
        '[%s:%d][%d:%s][%s]',
        basename($errfile),
        $errline,
        $errno,
        $errstr,
        print_r($errcontext, true)
    );
}, null);

$web_turbine->bootstrap();
