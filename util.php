<?php
function redirect() {
    call_user_func_array('pingpong\redirect', func_get_args());
}

function abort() {
    call_user_func_array('pingpong\abort', func_get_args());
}

function autoload() {
    return call_user_func_array(
        array('PingPong\PingPong', 'autoload'),
        func_get_args()
    );
}

function load() {
    var_dump(func_get_args());
    return call_user_func_array(
        array('PingPong\PingPong', 'load'),
        func_get_args()
    );
}

function notmodified() {

}

function notfound() {

}

function template() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'template'),
        func_get_args()
    );
}

function route() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'route'),
        func_get_args()
    );
}

function get() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'get'),
        func_get_args()
    );
}

function post() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'post'),
        func_get_args()
    );
}

function put() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'put'),
        func_get_args()
    );
}

function delete() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'delete'),
        func_get_args()
    );
}

function error() {
    $environ = PingPong\Environ::getInstance();

    return call_user_func_array(
        array($environ['pingpong.app'], 'error'),
        func_get_args()
    );
}
