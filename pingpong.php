<?php
// TODO: handle error
// TODO: log
// TODO: autojson

/**
 * @file pingpong.php
 * @brief micro framework
 * @author Yuan B.J. the_guy_1987@hotmail.com
 * @version 1.0
 * @date 2012-04-17
 */
namespace PingPong;

use \Exception;
use \ArrayIterator;

use \IteratorAggregate;
use \arrayaccess;
use \Countable;

$HTTP_CODES = array(
    // Informational 1xx
    100 => '100 Continue',
    101 => '101 Switching Protocols',
    // Successful 2xx
    200 => '200 OK',
    201 => '201 Created',
    202 => '202 Accepted',
    203 => '203 Non-Authoritative Information',
    204 => '204 No Content',
    205 => '205 Reset Content',
    206 => '206 Partial Content',
    // Redirection 3xx
    300 => '300 Multiple Choices',
    301 => '301 Moved Permanently',
    302 => '302 Found',
    303 => '303 See Other',
    304 => '304 Not Modified',
    305 => '305 Use Proxy',
    306 => '306 (Unused)',
    307 => '307 Temporary Redirect',
    // Client Error 4xx
    400 => '400 Bad Request',
    401 => '401 Unauthorized',
    402 => '402 Payment Required',
    403 => '403 Forbidden',
    404 => '404 Not Found',
    405 => '405 Method Not Allowed',
    406 => '406 Not Acceptable',
    407 => '407 Proxy Authentication Required',
    408 => '408 Request Timeout',
    409 => '409 Conflict',
    410 => '410 Gone',
    411 => '411 Length Required',
    412 => '412 Precondition Failed',
    413 => '413 Request Entity Too Large',
    414 => '414 Request-URI Too Long',
    415 => '415 Unsupported Media Type',
    416 => '416 Requested Range Not Satisfiable',
    417 => '417 Expectation Failed',
    418 => 'I\'m a teapot', // RFC 2324
    422 => '422 Unprocessable Entity',
    423 => '423 Locked',
    428 => '428 Precondition Required',
    429 => '429 Too Many Requests',
    431 => '431 Request Header Fields Too Large',
    // Server Error 5xx
    500 => '500 Internal Server Error',
    501 => '501 Not Implemented',
    502 => '502 Bad Gateway',
    503 => '503 Service Unavailable',
    504 => '504 Gateway Timeout',
    505 => '505 HTTP Version Not Supported',
    511 => '511 Network Authentication Required'
);

/**
 * StatusLine
 *
 * @param mixed $status_code http statu code for response
 *
 * @access public
 * @return void
 */
function statusline($status_code) {
    return $GLOBALS['HTTP_CODES'][$status_code];
}

/**
 * PingPong
 *
 * @package placeholder
 * @version 1.0.0
 * @copyright Copyright 2011 by Your Name <the_guy_1987@hotmail.com>
 * @author B.J. Yuan <the_guy_1987@hotmail.com>
 * @license Provided under the GPL (http://www.gnu.org/copyleft/gpl.html)
 */
class PingPong {
    private $error_handler_ = array();
    private $hooks_ = array();
    private $router_;
    private $view_;

    private $catchall_;
    private $autojson_;

    private static $autoload_class_ = array();
    private static $load_class_ = array();

    public function __construct(ConfigBag $config) {
        $autoload_class = &self::$autoload_class_;

        spl_autoload_register(
            function($name) use(&$autoload_class) {
                if (isset($autoload_class[$name])) {
                    require $autoload_class[$name];
                }
            }
        );

        $this->catchall_ = $config->get('catchall', true);
        $this->autojson_ = $config->get('autojson', true);
        $this->router_ = new Router();

        $view_config = $config->get('view');

        if ($view_config) {
            $this->view($view_config['type'], $view_config['settings']);
        }
    }

    public static function load() {
        $args = func_get_args();
        $cnt = count($args);

        if (1 == $cnt) {
            foreach ($args[0] as $class => $file) {
                if (!isset(self::$load_class_[$class])) {
                    self::$load_class_[$class] = $file;
                    // TODO: if $file not exists ?
                    // ANS: it should be there, if you config it
                    require $file;
                }
            }
        } elseif ($cnt > 1) {
            if (!isset(self::$load_class_[$args[0]])) {
                self::$load_class_[$args[0]] = $args[1];
                require $args[1];
            }
        }
    }

    public static function autoload() {
        $args = func_get_args();
        $cnt = count($args);

        if (1 == $cnt) {
            foreach ($args[0] as $class => $file) {
                // if not exists ?
                self::$autoload_class_[$class] = $file;
            }
        } elseif ($cnt > 1) {
            self::$autoload_class_[$args[0]] = $args[1];
        }
    }

    public function route($path,
                          $callable,
                          $method = 'GET',
                          $name = null) {
        if (!is_array($path)) {
            $path = array($path);
        }

        if (!is_array($method)) {
            $method = array($method);
        }

        foreach ($path as $rule) {
            foreach ($method as $verb) {
                $verb = strtoupper($verb);
                $route = new Route($rule, $callable, $verb, $name);
                $this->addRoute($route);
            }
        }

        return $this;
    }

    public function get($path,
                        $callable,
                        $name = null) {
        return $this->route($path, $callable, 'GET', $name);
    }

    public function post($path,
                         $callable,
                         $name = null) {
        return $this->route($path, $callable, 'POST', $name);
    }

    public function put($path,
                        $callable,
                        $name = null) {
        return $this->route($path, $callable, 'PUT', $name);
    }

    public function delete($path,
                           $callable,
                           $name = null) {
        return $this->route($path, $callable, 'DELETE', $name);
    }

    public function error($handler, $code = 500) {
        $this->error_handler_[$code] = $handler;
        return $this;
    }

    // TODO: hook
    public function hook() {

    }

    public function addRoute(Route $route) {
        $this->router_->add($route);
    }

    public function match($environ) {
        return $this->router_->match($environ);
    }

    public function handle_($environ) {
        $request = new HTTPRequest($environ);

        try {
            $route = $this->match($environ);
            $ret = $route->call($request);

            return $ret;
        } catch(HTTPResponse $e) {
            return $e;
        } catch(Exception $e) {
            if (!$this->catchall_) {
                throw $e;
            }

            return new HTTPError(500, 'Internal Server Error', $e);
        }
    }

    public function cast_($out) {
        $ret = '';

        if (empty($out)) {
            $ret = new HTTPResponse();
        } elseif (is_string($out)) {
            $ret = new HTTPResponse($out);;
        } elseif (is_array($out)) {
            $ret = $this->autojson_ ?
                    new JSONResponse($out) : new HTTPResponse(implode('', $out));
        } elseif ($out instanceof HTTPError) {
            $ret = new HTTPResponse(
                isset($this->error_handler_[$out->status_code]) ?
                    $this->error_handler_[$out->status_code]($out)
                        : statusline($out->status_code),
                $out->status_code
            );
        } elseif ($out instanceof HTTPResponse) {
            $ret = $out;
        } else {
            // TODO: response type not supported
        } // impossible reached

        return $ret;
    }

    public function template($template, $data = null) {
        return $this->view_->render($template, $data);
    }

    public function view($view, $settings = null) {
        $class = 'PingPong\\' . ucfirst($view) . 'Template';
        $this->view_ = new $class($settings);
    }

    public function __invoke($environ) {
        return $this->cast_($this->handle_($environ));
    }
}

/**
 * Environ
 *
 * @package placeholder
 * @version 1.0.0
 * @copyright Copyright 2011 by Your Name <the_guy_1987@hotmail.com>
 * @author B.J. Yuan <the_guy_1987@hotmail.com>
 * @license Provided under the GPL (http://www.gnu.org/copyleft/gpl.html)
 */
class Environ extends ArrayIterator {
    private $headers_;

    private static $special_keys_ = array(
        'CONTENT_LENGTH',
        'CONTENT_TYPE',
        'PHP_AUTH_USER',
        'PHP_AUTH_PW',
        'PHP_AUTH_DIGEST',
        'AUTH_TYPE'
    );

    private static $instance_ = null;

    public static function getInstance() {
        if (self::$instance_) {
            return self::$instance_;
        }

        return self::$instance_ = new static;
    }

    public static function mock($raw) {
        return self::$instance_ = new static($raw);
    }

    public function __construct($raw = null) {
        parent::__construct($raw ? $raw : $_SERVER);

        $this['GET'] = $_GET;
        $this['POST'] = $_POST;
        $this['REQUEST'] = $_REQUEST;
        $this['COOKIE'] = $_COOKIE;
    }

    public function get($name, $default = null) {
        return isset($this[$name]) ? $this[$name] : $default;
    }

    public function getHeaders() {
        if ($this->headers_) {
            return $this->headers_;
        }

        foreach ($this as $key => $var) {
            if ('HTTP_' == substr($key, 0, 5)) {
                $this->headers_[$key] = $var;
            } elseif (in_array($key, self::$special_keys_)) {
                $this->headers_[$key] = $var;
            }
        }

        return $this->headers_;
    }
}

class HTTPHeaders extends ArrayIterator implements Countable {
    private $environ_;

    public function __construct($headers) {
        parent::__construct($headers);
    }

    public function count() {
        return count($this);
    }

    public function get($name, $default = null) {
        $name = $this->ekey_($name);

        return isset($this->environ_[$name]) ?
                $this->environ_[$name] : $default;
    }

    public function has($name) {
        return isset($this[$name]);
    }

    public function merge($headers) {
        foreach ($headers as $name => $value) {
            $this[$name] = $value;
        }
    }
}

// route
class Route {
    public $rule;
    public $method;
    public $callable;
    public $name;
    public $config;

    public function __construct($rule,
                                $callable,
                                $method,
                                $name = null,
                                $config = null) {
        $this->rule = $rule;
        $this->method = $method;
        $this->callable = $callable;
        $this->name = $name ? $name : null;
        $this->config = new ConfigBag($config);
    }

    public function call($request) {
        $callable = $this->callable;
        return $callable($request);
    }

    private function context() {
        return array(
            'rule' => $this->rule,
            'method' => $this->method,
            'callable' => $this->callable,
            'name' => $this->name,
            'config' => $this->config
        );
    }
}

// router
class Router {
    // now only static rule, and just for path_info
    private $rules_ = array();
    private $static_ = array();

    public function add(Route $route) {
        $this->rules_[$route->rule] = array($route->method => $route);
        $this->static_[$route->rule] = array($route->method => $route);
    }

    public function match(Environ $environ) {
        $path = isset($environ['PATH_INFO']) ? $environ['PATH_INFO'] : '/';
        list($routes, $urlargs) = array(null, array());

        if (isset($this->static_[$path])) {
            $routes = $this->static_[$path];
        }

        if (!$routes) {
            throw new HTTPError(404, 'Not found: ' . $environ['REQUEST_URI']);
        }

        // foreach ($this->rules_ as $rule => $route) {
            // if (is_callable($rule)) {
                // if ($rule($environ)) {
                    // $match = $route;
                    // break;
                // }
            // } elseif ('*' == $rule) {
                // $match = $route;
                // break;
                // // other rule choice
            // }
        // }

        $method = strtoupper($environ['REQUEST_METHOD']);

        if (isset($routes[$method])) {
            return $routes[$method];
        }

        if ('HEAD' == $method || isset($routes['GET'])) {
            return $routes[$method];
        }

        if (isset($routes['ANY'])) {
            return $routes['ANY'];
        }

        $allowed = array_keys($routes);

        throw new HTTPError(
            405, 'Method not allowed.', array('Allow' => implode(',', $allowed)));
    }
}

/**
 * HTTPResponse
 *
 * @package placeholder
 * @version 1.0.0
 * @copyright Copyright 2011 by Your Name <the_guy_1987@hotmail.com>
 * @author B.J. Yuan <the_guy_1987@hotmail.com>
 * @license Provided under the GPL (http://www.gnu.org/copyleft/gpl.html)
 */
class HTTPResponse extends Exception implements arrayaccess, IteratorAggregate {
    private $body_;

    private $status_line_;
    private $status_code_;
    private $headers_;

    public static $default_content_type = 'text/html; charset=UTF-8';
    public static $default_status_code = 200;

    public function __construct($body = '', $status = NULL, $headers = NULL) {
        $this->headers_ = $headers ?
            new HTTPHeaders($headers)
            : new HTTPHeaders(
                    array('Content-Type' => self::$default_content_type));

        $this->status_code = $status ? (int)$status : self::$default_status_code;
        $this->body = $body;
    }

    public function getIterator() {
        return $this->headers_;
    }

    public function offsetExists($offset) {
        return isset($this->headers_[$offset]);
    }

    public function offsetGet($offset) {
        return $this->headers_[$key];
    }

    public function offsetSet($offset, $value) {
        $this->headers_[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->headers_[$offset]);
    }

    public function count() {
        return count($this->headers_);
    }

    public function __get($name) {
        switch ($name) {
            case 'status_code':
                return $this->status_code_;
            case 'status_line':
                return $this->status_line_;
            case 'headers':
                return $this->headers_;
            case 'body':
                return $this->body_;
            case 'charset':
                if ($charset = stristr($this['Content-Type'], 'charset=')) {
                    return end(substr($charset, 8)).explode(';', 1);
                }

                return '';
        }
    }

    public function __set($name, $value) {
        if ('status_code' == $name) {
            $this->status_code_ = $value;
            $this->status_line_ = statusline($value);

            if ($value > 999 || $value < 100) {
                // TODO: internal error
            }
        } elseif ('body' == $name) {
            $this->body_ = $value;
            $this['Content-Length'] = strlen($value);
        }
    }

    public function getHeader($name, $default = null) {
        return $this->headers->get($name, $value);
    }

    public function addHeader($name, $value) {
        $this->headers_[$name] .= $value;
    }

    public function setHeader($name, $value, $append = false) {
        if ($append) {
            $this->addHeader($name, $value);
        } else {
            $this->headers_[$name] = $value;
        }
    }

    public function redirect($url, $status = 302) {
        $this->status_code = $status;
        $this['Location'] = $url;
    }

    // TODO: deal with cookie
}

class HTTPError extends HTTPResponse {
    public function __construct($status = 500, $body = '', $headers = null) {
        parent::__construct($body, $status, $headers);
    }

    public function __toString() {
        return $this->status_line;
    }
}

class JSONResponse extends HTTPResponse {
    public function __construct($jsonable, $status = NULL ,$headers = NULL) {
        parent::__construct(
            json_encode($jsonable),
            $status,
            array('Content-Type' => 'application/json')
        );
    }

    public function __toString() {
        return $this->body;
    }
}

class ConfigBag extends ArrayIterator {
    public function __construct($a = null) {
        if ($a) {
            $this($a);
        }
    }

    public function set($key, $value) {
        $this[$key] = $value;
    }

    public function get($key, $default = null) {
        if (isset($this[$key])) {
            return $this[$key];
        }

        return $default;
    }

    public function del($key) {
        unset($this[$key]);
    }

    public function __invoke($a) {
        if (is_array($a)) {
            foreach ($a as $key => $value) {
                $this[$key] = $value;
            }
        }

        return $this;
    }
}

class ParameterBag {
    private $parameters_;

    public function __construct($parameters) {
        $this->parameters_ = $parameters;
    }

    public function get($name, $default = null) {
        return isset($this->parameters_[$name]) ?
                $this->parameters_[$name] : $default;
    }

    public function set($name, $value) {
        $this->parameters_[$name] = $value;
    }

    public function has($name) {
        return isset($this->parameters_[$name]);
    }
}

class HTTPRequest {
    const METHOD_HEAD       = 'HEAD';
    const METHOD_GET        = 'GET';
    const METHOD_POST       = 'POST';
    const METHOD_PUT        = 'PUT';
    const METHOD_DELETE     = 'DELETE';
    const METHOD_OPTIONS    = 'OPTIONS';

    private $container_ = array();
    private $environ_;
    public $GET;
    public $POST;

    public function __construct($environ) {
        $this->environ_ = $environ;
        $this->GET = new ParameterBag($environ['GET']);
        $this->POST = new ParameterBag($environ['POST']);
    }

    public function getHeader($name, $default = null) {
        return $this['headers']->get($name, $default);
    }

    // TODO: getBody
    public function getBody() {

    }

    public function getCookie() {

    }

    public function params($name = null, $default = null) {
        $ret = $this->POST->get($name, $default);

        if (is_null($ret)) {
            $ret = $this->GET->get($name, $default);
        }

        return $ret;
    }

    public function get($name = null, $default = null) {
        if ($name) {
            return $this->GET->get($name, $default);
        }

        return $this->GET;
    }

    public function post($name = null, $default = null) {
        if ($name) {
            return $this->POST->get($name, $default);
        }

        return $this->POST;
    }

    // TODO cookie
    public function cookie() {

    }

    public function __get($name) {
        if (isset($this->container_[$name])) {
            return $this->container_[$name];
        }

        $value = '';

        switch($name) {
            case 'headers':
                $value = new HTTPHeaders($this->environ_->getHeaders());
                break;
            // TODO: files
            case 'files':
                break;
            case 'content_length':
                $value = $this['headers']->get('CONTENT_LENGTH', -1);
                break;
            case 'is_xhr':
                $value = (
                    'xmlhttprequest' ==
                    strtolower(
                        $this->getHelper_(
                            'is_xhr',
                            $this->environ_->get('HTTP_X_REQUESTED_WITH', '')
                        )
                    )
                );
                break;
            case 'is_ajax':
                $value = $this->is_xhr;
                break;
            case 'remote_addr':
                // TODO fill the remote addr
                return '';
            case 'remote_route':
                // TODO
                return '';
            default:
                $value = $this['headers']->get(strtoupper($name), '');
        }

        return $this->container_[$name] = $value;
    }

    public function __set($name, $value) {
        // server environ parameters can not be modified
    }
}

abstract class BaseTemplate {
    protected $options_ = array();
    protected $tpl_ = null;
    protected $data_ = array();

    public function __construct($options = array()) {
        $this->options_ = array_merge($this->options_, $options);
        $this->prepare($this->options_);
    }

    public function getData($key = null) {
        if (is_null($key)) {
            return $this->data_;
        }

        return isset($this->data_[$key]) ? $this->data_[$key] : null;
    }

    public function setData() {
        $args = func_get_args();
        $cnt = count($args);

        if (1 == $cnt && is_array($args[0])) {
            $this->data_ = $args[0];
        } elseif (2 == $cnt) {
            $this->data_[$args[0]] = $args[1];
        } else {
            // TODO: exception here
        }
    }

    public function mergeData(array $data) {
        $this->data_ = array_merge($this->data_, $data);
    }

    abstract function prepare($options);

    abstract function render($template, $args);

    public function display($template) {
        echo $this->render($template);
    }
}

class SmartyTemplate extends BaseTemplate {
    public function prepare($options) {
        $this->tpl_ = new \Smarty($options);
    }

    public function render($template, $data = null) {
        if ($data) {
            if (!is_array($data)) {
                $data = array($data) ;
            }

            $this->data_ = array_merge($this->data_, $data);
            $this->tpl_->assign($data);
        }

        return $this->tpl_->fetch($template);
    }
}

// TODO: throw it into trash bin ?
function view($tpl) {
    $tpl = ucfirst($tpl);

    return function($options) use($tpl) {
        $name = $tpl . 'View';
        $template_adapter = new $name($options);

        return function($template, $data) use($template_adapter) {
            return $template_adapter->render($template, $data);
        };
    };
}

$pingpong_smarty_view = view('smarty');

function abort($status_code = 500, $body = '500 Internal Server Error') {
    throw new HTTPError($status_code, $body);
}

function redirect($url, $status_code = null) {
    if (empty($status_code)) {
        if ('HTTP/1.1' == Environ::getInstance()->get('SERVER_PROTOCOL')) {
            $status_code = 303;
        } else {
            $status_code = 302;
        }
    }

    throw new HTTPResponse('', $status_code, array('Location' => $url));
}

// TODO
function static_file() {

}

function run() {

}

function app($settings) {
    $environ = Environ::getInstance();
    $config = $environ['pingpong.config'] = new ConfigBag($settings);
    return $environ['pingpong.app'] = new PingPong($config);
}

$environ = Environ::getInstance();
